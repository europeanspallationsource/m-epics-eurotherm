epicsEnvSet(MODBUS_NET,  192.168.111.222:502)
epicsEnvSet(MB_PORT0R,   RIOMODPV)
epicsEnvSet(MB_PORT0W,   WIOMODPV)
epicsEnvSet(MB_PORT1R,   RLOOP)
epicsEnvSet(MB_PORT1W,   WLOOP)
epicsEnvSet(ASYN_PORT,   EUROTHERM)
epicsEnvSet(PREFIX,      EUROTHERM)

require eurotherm,0.1.2

# Setup asyn IP port
drvAsynIPPortConfigure(${ASYN_PORT}, ${MODBUS_NET}, 0, 0, 1)

# Enable debug trace
#asynSetTraceMask(${ASYN_PORT}, 0, 0x29)

# Setup Modbus Interpose on asyn port
modbusInterposeConfig(${ASYN_PORT}, 0, 0, 0)

# Setup Modbus ports
# The IO modules PV variables are stored at addresses 4228-4259.
# The Loop 1 variables are stored at addresses 1-123
# Poll ports with 1000 ms intervals
# Read ports
drvModbusAsynConfigure(${MB_PORT0R}, ${ASYN_PORT}, 1, 3, 4228, 32, 0, 1000, "")
drvModbusAsynConfigure(${MB_PORT1R}, ${ASYN_PORT}, 1, 3, 1,  122, 0, 1000, "")
# Write ports
drvModbusAsynConfigure(${MB_PORT0W}, ${ASYN_PORT}, 1, 6, 4228, 32, 0, 1000, "")
drvModbusAsynConfigure(${MB_PORT1W}, ${ASYN_PORT}, 1, 6, 1,  122, 0, 1000, "")

dbLoadRecords(eurothermIoModPV.db, "PREFIX=${PREFIX},  PORT_READ=${MB_PORT0R}, PORT_WRITE=${MB_PORT0W}")
dbLoadRecords(eurothermLoop.db,    "PREFIX=${PREFIX},  PORT_READ=${MB_PORT1R}, PORT_WRITE=${MB_PORT1W}, LOOP_ID=1")

