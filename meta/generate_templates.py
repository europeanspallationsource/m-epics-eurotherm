#!/usr/bin/env python
from __future__ import print_function

configure = ""
load_template = ""

# There are always 8 IO modules.
# To reuse this script for another setup you need to adapt this tuple:
_io_mods = ('ThermInput', 'ThermInput', 'ThermInput', 'ThermInput', '', '', 'LogicOutput', 'LogicOutput')

_params = [ 
	{ 'name': 'PV', 'type': 'Ao' },
	{ 'name': 'AlarmAck', 'type': 'Longout' },
	{ 'name': 'MinOnTime', 'type': 'MoT' },
	{ 'name': 'LoPoint', 'type': 'Ao' },
	{ 'name': 'LoOffset', 'type': 'Ao' },
	{ 'name': 'HiPoint', 'type': 'Ao' },
	{ 'name': 'HiOffset', 'type': 'Ao' }
]

for (parameter_i, parameter) in enumerate(_params):
    with open('eurothermIoMod{}.subs'.format(parameter['name']), 'w') as fh:
        for (iomod_i, iomod) in enumerate(_io_mods):
            if parameter['name'] == 'PV' and iomod:
                fh.write('file eurotherm{}.template {{\n\tpattern {{MOD_ID, R, OFFSET}}\n'.format(iomod))
                for offset in range(iomod_i*4, iomod_i*4 + 4):
                    fh.write('\t\t{{{}, IO-Mod-$(MOD_ID)-{}, {}}}\n'.format(offset + 1, parameter['name'], offset))
                fh.write('}\n')
            fh.write('\n')

    configure = configure + 'drvModbusAsynConfigure(${{MB_PORT{}R}}, ${{ASYN_PORT}}, 1, 3, {}, 32, 0, 1000, "")\n'.format(parameter_i, 4228 + 32 * parameter_i)
    configure = configure + 'drvModbusAsynConfigure(${{MB_PORT{}W}}, ${{ASYN_PORT}}, 1, 6, {}, 32, 0, 1000, "")\n'.format(parameter_i, 4228 + 32 * parameter_i)
    load_template = load_template + 'dbLoadTemplate(eurothermIoMod{}.db, "PREFIX=${{PREFIX}}, PORT_READ=${{MB_PORT{}R, PORT_WRITE=${{MB_PORT{}W}}")\n'.format(parameter['name'], parameter_i, parameter_i)


print(configure)
print(load_template)
